package com.nest.demo.external

object SetPath {

  def setPath(num: Double): Integer = {
    if (num <= .20) 1
    else if (num <= .4) 2
    else if (num <= .8) 3
    else if (num <= .95) 4
    else 5
    }
}


