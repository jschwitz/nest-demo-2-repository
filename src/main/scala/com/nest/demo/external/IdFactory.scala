package com.nest.demo.external

object IdFactory {
  private var counter = 0
  def create(): String = {
    counter += 1
    "mortgage_"+counter.toString
  }
}

