package com.nest.demo.external

object GetUniform {
  private val uniform = new breeze.stats.distributions.Uniform(0.0, 1.0)
  def uniformRandom(): Double = {
    uniform.sample
  }
}
