package com.nest.demo;

import com.nest.demo.external.ExecuteJava;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import com.nest.demo.external.*;

import java.util.logging.Logger;

public class MortgageAssign implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        // Assign mortgageID
        String mortgage = IdFactory.create();
        execution.setVariable("mortgage", "mortgage01");

        // Classify path
        Integer type_num = SetPath.setPath(GetUniform.uniformRandom());
        execution.setVariable("type_num", type_num);

    }

}