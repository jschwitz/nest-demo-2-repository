package com.nest.demo;

import java.util.logging.Logger;

import com.nest.demo.external.ExecuteJava;
import org.camunda.bpm.client.ExternalTaskClient;

public class ExecuteNoteAssignment {
    //private final static Logger LOGGER = Logger.getLogger(ExecuteNoteAssignment.class.getName());

    public static void main(String[] args) {
        ExternalTaskClient client = ExternalTaskClient.create()
                .baseUrl("http://localhost:8080/engine-rest")
                .asyncResponseTimeout(1000) // long polling timeout
                .build();

        // subscribe to an external task topic as specified in the process
        client.subscribe("Note-Assignment")
                .lockDuration(1000) // the default lock duration is 20 seconds, but you can override this
                .handler((externalTask, externalTaskService) -> {
                    // Put your business logic here

                    // Get a process variable
                    String item = (String) externalTask.getVariable("mortgage");

                    // Get Random Validated Boolean
                    Boolean Validated = (Boolean) ExecuteJava.getRandomBoolean(70f);
                    try {

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // Complete the task
                    externalTaskService.complete(externalTask);
                })
                .open();
        }
    }


/*




    public static void main(String[] args) {
        ExternalTaskClient client = ExternalTaskClient.create()
                .baseUrl("http://localhost:8080/engine-rest")
                .asyncResponseTimeout(1000) // long polling timeout
                .build();

        // subscribe to an external task topic as specified in the process
        client.subscribe("Note-Assignment")
                .lockDuration(1000) // the default lock duration is 20 seconds, but you can override this
                .handler((externalTask, externalTaskService) -> {
                    // Put your business logic here

                    // Get a process variables
                    String mortgage = (String) externalTask.getVariable("mortgage");
                    //Integer type_num = (Integer) externalTask.getVariable("type_num");
                    //LOGGER.info("Processing the following mortgage '" + mortgage + "'for type '" + type_num + "'...");
                    // Get Random Validated Boolean
                    //Boolean Validated = (Boolean) ExecuteJava.getRandomBoolean(70f);

                    // Complete the task
                    externalTaskService.complete(externalTask);
                })
                .open();
    }
}

*/