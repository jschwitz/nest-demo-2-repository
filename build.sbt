name := "demo2"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
  "org.camunda.bpm" % "camunda-engine" % "7.13.0-alpha3" % "provided",
  "org.camunda.bpm.extension" % "camunda-bpm-assert" % "1.2" % "test",
  "org.camunda.spin" % "camunda-spin-dataformat-all" % "1.8.0" % "test",
  "org.camunda.bpm" % "camunda-engine-plugin-spin" % "7.13.0-alpha3" % "test",
  "org.camunda.template-engines" % "camunda-template-engines-freemarker" % "2.0.0" % "test",
  "org.camunda.template-engines" % "camunda-template-engines-velocity" % "2.0.0" % "test",
  "junit" % "junit" % "4.12" % "test",
  "com.h2database" % "h2" % "1.4.197" % "test",
  "org.camunda.bpm.extension" % "camunda-bpm-process-test-coverage" % "0.3.2" % "test",
  "ch.qos.logback" % "logback-classic" % "1.1.3" % "test",
  "org.slf4j" % "jcl-over-slf4j" % "1.7.25" % "test",
  "org.slf4j" % "jul-to-slf4j" % "1.7.25" % "test",
  "com.lihaoyi" %% "requests" % "0.5.1",
  "org.scala-lang.modules" %% "scala-xml" % "1.2.0",
  "org.scalatest" %% "scalatest" % "3.3.0-SNAP2" % "test",
  "com.lihaoyi" %% "requests" % "0.5.2",
  "org.camunda.bpm" % "camunda-external-task-client" % "1.3.0" ,
  "javax.xml.bind" % "jaxb-api" % "2.3.1",
  "org.scalanlp" %% "breeze" % "1.0"
)

enablePlugins(JettyPlugin)